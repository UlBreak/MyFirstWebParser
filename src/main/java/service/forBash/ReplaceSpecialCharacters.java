package service.forBash;

import interfaces.forBah.RemovingExcess;

public class ReplaceSpecialCharacters implements RemovingExcess {
    public String removingExcess(String str) {
        int marker0 = str.indexOf("<div class=\"quote__body\">");
        int marker1 = str.indexOf("<footer class=\"quote__footer\">");
        String textQuote = str.substring(marker0 + 31, marker1);
        textQuote = textQuote.replaceAll("<br />", "\n").replaceAll("<br>", "\n");
        textQuote = textQuote.replaceAll("</div>", "");
        textQuote = textQuote.replaceAll("&lt;", "<").replaceAll("&gt;", ">");

        return textQuote;
    }

    @Override
    public String returnResult(String str) {
        return removingExcess(str);
    }
}
