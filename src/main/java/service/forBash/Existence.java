package service.forBash;

import console.InputIDForBash;

import java.io.IOException;

public class Existence implements interfaces.forBah.Existence {

    public boolean existence(String testNum) throws IOException {
        InputIDForBash inputID = new InputIDForBash();
        boolean flag = false;
        int marker0 = inputID.returnPageForCheck(testNum).indexOf("Таких цитат не найдено!");
        if (marker0 == -1)
            flag = true;
        return flag;
    }

    @Override
    public boolean returnResult(String testNum) throws IOException {
        if (existence(testNum)) {
            return true;
        }
        return false;
    }
}
