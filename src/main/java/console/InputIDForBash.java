package console;

import getpage.*;

import java.io.IOException;


public class InputIDForBash {

    GetHTML getHTML = new GetHTML();

    public StringBuilder returnPage(String num) throws IOException {
        return getHTML.getPage("https://bash.im/quote/", num);
    }

    public StringBuilder returnPageForCheck(String testNum) throws IOException {
        return getHTML.getPage("https://bash.im/search?text=", testNum);
    }
}



