import console.InputIDForBash;
import service.forBash.*;

import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        Scanner scan = new Scanner(System.in);
        Existence existence = new Existence();
        ReplaceSpecialCharacters replace = new ReplaceSpecialCharacters();
        InputIDForBash inputID = new InputIDForBash();
        System.out.print("введите ID цитаты: ");
        String ID = scan.nextLine();
        if (existence.returnResult(ID)) {
            String quotes = String.valueOf(inputID.returnPage(ID));
            System.out.println(replace.returnResult(quotes));
        } else
            System.out.println("Цитаты под таким индексом не существует!");
    }
}

