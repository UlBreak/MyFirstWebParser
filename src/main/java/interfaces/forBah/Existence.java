package interfaces.forBah;

import java.io.IOException;

public interface Existence {
    boolean returnResult(String testNum) throws IOException;
}
