package getpage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class GetHTML {
    public StringBuilder getPage(String https, String num) throws IOException {
        String site = https + num;
        URL url = new URL(site);
        URLConnection conn = url.openConnection();
        BufferedReader br = new BufferedReader(
                new InputStreamReader(conn.getInputStream()));

        String textHtml;
        StringBuilder htmlSite = new StringBuilder();
        while ((textHtml = br.readLine()) != null) {
            htmlSite.append(textHtml);
        }
        br.close();
        return htmlSite;
    }
}
