Wrote a web parser for the site "https://bash.im/". The user enters a quote ID into the console, and the program displays give this quote in the console or reports if quotes with this ID don't exist.

Написал парсер для сайта "https://bash.im/". Пользователь вводит ID цитаты в консоль, а программа выводит саму цитату в консоли или сообщает если цитаты с таким ID не существует.